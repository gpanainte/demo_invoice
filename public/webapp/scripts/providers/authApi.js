
angular.module('demoInvoice').factory('authApi',['$rootScope','$http','serverUrl', function authApi($rootScope,$http,serverUrl){

  var isLoggedIn = function(response){
    var responseData = response.data;
    // console.log(responseData);
    if(responseData.status==="OK"){
      $rootScope.currentUser = responseData.data.fullName;
    }
    return responseData;
  };

  var error = function(error){
    $rootScope.currentUser = null;
    return error.data;
  }

  var isLoggedOut = function(response){
    $rootScope.currentUser = null;
    Session.destroy();
  };

  authApi.validateLogin = function(username, password){

    return $http.post(serverUrl+'/login',{user:{name:username,password:password}}).then(isLoggedIn,error);

  };

  authApi.logout = function(){

    return $http.post(serverUrl+'/logout',{username:username,password:password}).then(isLoggedOut,error);

  };

authApi.isAuthenticated = function () {

  return $http.get(serverUrl+'/isLoggedIn');

  };

  return authApi;
}]);
