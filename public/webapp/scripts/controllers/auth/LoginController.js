angular.module('demoInvoice').controller('LoginController',['$scope','authApi','$state',function LoginController($scope,authApi,$state){

  $scope.errorMsg = null;

  var ok = function (response) {
    if(response.status==="OK"){
      $state.transitionTo("app.invoices");
    } else{
      $scope.errorMsg = response.data;
    }
  };

  var failed = function(error){
    console.log(error);
  };

  $scope.login = function(username,password){
      authApi.validateLogin(username,password).then(ok,failed);
  };


}]);
