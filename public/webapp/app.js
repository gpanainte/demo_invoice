var demoApp = angular.module('demoInvoice', ['ui.router']);

demoApp.config(['$stateProvider', '$urlRouterProvider',
    function config($stateProvider, $urlRouterProvider) {


        $stateProvider.state('auth',{
           url:'/auth',
           templateUrl:'webapp/templates/login_template.html',
           abstract:true,
           data:{
               requireLogin:false,
               css: 'login'
           } 
        }).state('auth.login',{
            url:'/login',
            views:{
                'content':{
                    templateUrl:'webapp/views/auth/login.html',
                    controller:'LoginController'
                }
            }
        }).state('app',{
            url:'/app',
            templateUrl:'webapp/templates/standard_template.html',
            abstract:true,
            data:{
                requireLogin:true,
                css:'custom'
            }
        }).state('app.invoices',{
            url:'/invoices',
            views:{
                'content':{
                    templateUrl:'webapp/views/invoice/invoice_list.html'
                }
            }
        }).state('app.editInvoice',{
            url:'/invoice/:id/edit',
            views:{
                'content':{
                    templateUrl:'webapp/views/invoice/edit_invoice.html'
                }
            }
        }).state('app.payments',{
            url:'/payments',
            views:{
                'content':{
                    templateUrl:'webapp/views/payment/payment_list.html'
                }
            }
        }).state('app.editPayment',{
            url:'/payment/:invoiceId/edit',
            views:{
                'content':{
                    templateUrl:'webapp/views/invoice/edit_payemnt.html'
                }
            }
        }).state('app.users',{
            url:'/users',
            views:{
                'content':{
                    templateUrl:'webapp/views/users/user_list.html'
                }
            }
        });
        
        $urlRouterProvider.otherwise('/auth/login');

    }]);


demoApp.run(['$rootScope', '$state', 'authApi',
  function($rootScope, $state, authApi) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {

      var requireLogin = toState.data.requireLogin;
      $rootScope.css = toState.data.css;

      if (requireLogin) {
        // $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);

        authApi.isAuthenticated().then(function(result){
          // console.log(result);
        },function(error){
          // console.log(error);
          $state.go('auth.login', toParams);
          event.preventDefault();
        });
      }
    });
  }
]);