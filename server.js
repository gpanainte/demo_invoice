var express = require("express"); //web application framework
var app = express();
var bodyParser = require("body-parser"); //body parsing middleware
var session = require('./server/auth/session');
var checkSession = require('./server/auth/check_session');


app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({
    'extended': 'true'
})); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({
    type: 'application/vnd.api+json'
})); // parse application/vnd.api+json as json

app.use(session); // session configuration
app.use(checkSession); //check session cookie


app.use('/api', require('./server/models/user/user_routes'));
app.use('/api', require('./server/models/invoice/invoice_routes'));
app.use('/api', require('./server/models/payment/payment_routes'));
app.use('/api', require('./server/auth/auth_routes.js'));

 app.get('*', function(req, res) {
   res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end).
 });

app.listen(8080, function () {
    console.log("server listening at 8080");
}); 
