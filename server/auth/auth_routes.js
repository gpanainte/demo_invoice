var route = require('express').Router();
var requireLogin = require.main.require('./server/auth/require_login');
var User = require.main.require('./server/models/user/user');
var _ = require('underscore');


route.get('/logout', function (request, response) {
  request.session.reset();
  response.send({
    status: 0,
    data: {}
  });
});

route.get('/isLoggedIn', requireLogin, function (request, response) {

  response.send({
    status: "OK",
    data: request.user
  });
});

route.post('/login', function (req, res) {

  var options = { columns: [], criteria: [] };
  var fields = User.getSchema().fields;


  options.columns = [fields.id.name, fields.name.name, fields.role.name, fields.password.name];
  options.criteria.push({ [fields.name.name]: req.body.user.name });

  User.find(options).then(function (user) {
    user = user[0];

    if (user && user.password == req.body.user.password) {
      req.user = user;
      req.session.user = user;
      res.locals.user = user;

      res.send({
        status: "OK",
        data: user
      });
    } else {
      return res.status(401).send({
        status: "NOK",
        data: "Login failed"
      });
    }

  }).catch(function (err) {
    return res.status(401).send({
      status: "NOK",
      data: err
    });

  });
});

module.exports = route;
