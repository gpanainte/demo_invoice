var User = require.main.require("./server/models/user/user");
var _ = require('underscore');
var checkSession = function (req, res, next) {

    var options = { columns: [], criteria: [] };
    var fields = User.getSchema().fields;

    if (!_.isEmpty(req.session) && !_.isEmpty(req.session.user)) {

        options.columns = [fields.id.name, fields.name.name, fields.role.name];
        options.criteria.push({ [fields.name.name]: req.session.user.name });
        options.criteria.push({agrCondition:"AND"});
        options.criteria.push({ [fields.role.name]: req.session.user.role });
        

        User.find(options).then(function (user) {
            user = user[0];
            if (user) {
                req.user = user;
                req.session.user = user;
                res.locals.user = user;
            }  
            
            next();
            
        }).catch(function (error) {
            next();
        });
    } else {
        next();
    }

};

module.exports = checkSession;
