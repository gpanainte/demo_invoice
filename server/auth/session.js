var session = require("client-sessions");


module.exports = session({
  cookieName: 'session',
  secret: 'adjalskdjakeqwei7w89ehd3quy487xdsnjkd3r8u3r8wjnzskflfhd', //string to encrypt cookie session
  duration: 30 * 60 * 1000, //how long the session will live in milliseconds
  activeDuration: 5 * 60 * 1000, //lengthen session by interacting with the site
});;
