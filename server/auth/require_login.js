var _ = require('underscore');

var requireLogin = function(req, res, next) {

  if (!req.user) {

    req.session.reset();

    return res.status(401).send({
      status: "NOK",
      data: "You must login"
    });

  }

  next();

};

module.exports = requireLogin;
