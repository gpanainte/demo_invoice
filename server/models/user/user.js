module.exports = User;

var db = require.main.require("./server/providers/db_handler").pool;
var q = require("q");
var _ =require('underscore');

const schema = {
    table : "users",
    fields : {
        id : {
            name:"id",
            required:true
        },
        name : {
            name : "name",
            required: true
        },
        password : {
            name:"password",
            required: true
        },
        role : {
            name:"role",
            required: true
        }
    }
}

//constructor
function User(name,password,role){
    
    var user = this;
    
    user.name = name;
    user.password = password;
    user.role = role;
    
    console.log(user);
}

/**
 * Creates an object where property names are same as table column names
 */
var convertToDatabaseColumnNames = function(user){
    var data = {};
    
    data[schema.fields.name.name] = user.name;
    data[schema.fields.password.name] = user.password; 
    data[schema.fields.role.name] = user.role;
    
    return data;
}

var convertToSchema = function(rawData){
    var data = {};
    
    _.each(_.keys(schema.fields),function(field){
        data[field] = rawData[schema.fields[field].name];
    });
    
    return data;
}

User.prototype.save = function(){
    
    var user = this;
    var data = {};
    var deferrer = q.defer();
    
    data[schema.fields.name.name] = user.name;
    data[schema.fields.password.name] = user.password; 
    data[schema.fields.role.name] = user.role; 
    
    db.getConnection(function(error,connection){
        if(error) {
            deferrer.reject(error);
        }
        
        connection.query("INSERT INTO ?? SET ?",[schema.table,data], function(error,result){
            connection.release();
            
            if(error) {
                deferrer.reject(error);
            }
            
            deferrer.resolve(result);
        });
    });

    return deferrer.promise;    
}

User.prototype.delete = function(){
    var user = this;
    var deferrer = q.defer();
    
    db.getConnection(function(error,connection){
       if(error){
           deferrer.reject(error);
       } 
       
       connection.query("DELETE FROM ?? WHERE ??=?",[schema.table,schema.fields.id.name,user.id],function(error,result){
           connection.release();
           
           if(error){
               deferrer.reject(error);
           }
           
           deferrer.resolve(result);
       });
    });
    
    return deferrer.promise;
}

User.prototype.update = function(){
    var user = this;
    var data = {};
    var deferer = q.defer();
    
    
    data = convertToDatabaseColumnNames(user);
    
    db.getConnection(function(error,connection){
        connection.release();
        
        if(error){
            deferer.reject(error);
        }
        
        connection.query("UPDATE ?? SET ? WHERE ??=?",[schema.table,data,schema.fields.id.name,user.id],function(error, result){
            if(error){
                deferrer.reject(error);
            }
            
            deferer.resolve(result);
        });
    });
    
    return deferer.promise;
}

User.findById = function(userId){
    var deferer = q.defer();
    
    db.getConnection(function(error,connection){
        if(error){
            deferer.reject(error);
        }
        
        connection.query("SELECT * FROM ?? WHERE ??=?",[schema.table,schema.fields.id.name,userId],function (error,user) {
           connection.release();
           
           if(error){
               deferer.reject(error);
           } 
           
           deferer.resolve(user);
        });
    });
    
    return deferer.promise;
}

User.findAll = function(){
    
    var deferer = q.defer();
    
    db.getConnection(function (error,connection) {
        if(error){
            deferer.reject(error);
        }
        
        connection.query("SELECT * FROM ??",[schema.table],function (error,users) {
            connection.release();
            
            if(error){
                deferer.reject(error);
            }
            
            deferer.resolve(users);
        });
    });
   
   return deferer.promise; 
}

User.find = function (options) {
    var deferer = q.defer();

    var queryString = "SELECT " + (options.columns.length > 0 ? "??" : "*") + " FROM ?? WHERE ";
    var queryData = [];

    if (options.columns.length > 0) {
        queryData.push(options.columns);
    }
    queryData.push(schema.table);

    for (condition of options.criteria) {
        if (_.has(condition, "agrCondition")) {
            queryString = queryString + " " + condition.agrCondition;
        } else {
            queryString = queryString + " ? ";
            queryData.push(condition);
        }
    }

    db.getConnection(function (error, connection) {
        if (error) {
            deferer.reject(error);
        }

        connection.query(queryString, queryData, function (error, users) {
            connection.release();

            if (error) {
                deferer.reject(error);
            }
            
            for(var i = 0; i<users.length; i++){
                users[i]=convertToSchema(users[i]);
            }

            deferer.resolve(users);
        });
    });

    return deferer.promise;
}


User.getSchema = function(){
    return schema;
}