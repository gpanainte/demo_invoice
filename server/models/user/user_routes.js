var route = require('express').Router();
var User = require.main.require('./server/models/user/user');
var Invoice = require.main.require('./server/models/invoice/invoice');
var InvoiceLines = require.main.require('./server/models/invoice_line/invoice_line');
var Payment = require.main.require('./server/models/payment/payment');
var requireLogin = require.main.require('./server/auth/require_login');

module.exports = route;

route.post('/user',requireLogin, function (request, response) {
    var user = new User();

    user.name = request.body.name;
    user.password = request.body.password;
    user.role = request.body.role;

    user.save().then(function (result) {
        response.send({ status: "OK", data: result });
    }).catch(function (error) {
        response.send({ status: "NOK", data: error });
    });

});

route.put('/user/:id',requireLogin, function (request, response) {

    var user = new User();

    user.id = request.params.id;
    user.name = request.body.name;
    user.password = request.body.password;
    user.role = request.body.role;

    user.update().then(function (result) {
        response.send({ status: "OK", data: result });
    }).catch(function (error) {
        response.send({ status: "NOK", data: error });
    });
});

route.get('/user/:id',requireLogin, function (request, response) {
    User.findById(request.params.id).then(function (user) {
        response.send(user);
    }).catch(function (error) {
        response.send(error);
    });
});

route.get('/users',requireLogin, function (request, response) {
    User.findAll().then(function (users) {
        response.send(users);
    }).catch(function (error) {
        response.send(error);
    });
});


route.get('/user/:id/invoices',requireLogin, function (request, response) {

    var fields = Invoice.getSchema().fields;
    var options = { columns: [], criteria: [] };

    options.criteria.push({ [fields.clientId.name]: request.params.id });

    Invoice.find(options).then(function (invoices) {
        response.send(invoices);
    }).catch(function (error) {
        response.send(error);
    });
});

route.get('/user/:id/invoice/:invoiceId',requireLogin, function (request, response) {

    var fields = Invoice.getSchema().fields;
    var options = { columns: [], criteria: [] };

    options.criteria.push({ [fields.id.name]: request.params.invoiceId });
    options.criteria.push({ agrCondition: "AND" });
    options.criteria.push({ [fields.clientId.name]: request.params.id });

    Invoice.find(options).then(function (invoice) {
        if (invoice) {
            Invoice.getInvoiceWithLines(invoice[0].id).then(function (invoiceWithLines) {
                response.send(invoiceWithLines);
            }).catch(function (error) {
                response.send(error);
            });
        } else{
            response.send(invoice);
        }
    }).catch(function (error) {
        response.send(error);
    });
});


route.get('/user/:id/payments',requireLogin, function (request, response) {

    var fields = Payment.getSchema().fields;
    var options = { columns: [], criteria: [] };

    options.criteria.push({ [fields.clientId.name]: request.params.id });

    Payment.find(options).then(function (invoices) {
        response.send(invoices);
    }).catch(function (error) {
        response.send(error);
    });
});
