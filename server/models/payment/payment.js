module.exports = Payment;

var db = require.main.require("./server/providers/db_handler").pool;
var q = require("q");
var _ = require("underscore");
var Invoice = require.main.require('./server/models/invoice/invoice');

const schema = {
    table: "payment",
    fields: {
        id: {
            name: "id",
            required: true
        },
        invoiceId: {
            name: "invoiceId",
            required: true
        },
        clientId: {
            name: "clientId",
            required: true
        },
        amount: {
            name: "amount",
            required: true
        },
        date: {
            name: "date",
            required: true
        }
    }
}

//constructor
function Payment(invoiceId, clientId, amount) {

    var payment = this;

    payment.invoiceId = invoiceId;
    payment.clientId = clientId;
    payment.amount = amount;
    payment.date = new Date();

}

/**
 * Creates an object where property names are same as table column names
 */
var convertToDatabaseColumnNames = function (payment) {
    var data = {};

    data[schema.fields.invoiceId.name] = payment.invoiceId;
    data[schema.fields.clientId.name] = payment.clientId;
    data[schema.fields.amount.name] = payment.amount;
    data[schema.fields.date.name] = payment.date;

    return data;
}

var convertToSchema = function(rawData){
    var data = {};
    
    _.each(_.keys(schema.fields),function(field){
        data[field] = rawData[schema.fields[field].name];
    });
    
    return data;
}

var updateInvoicePayedAmount = function (invoiceId, paidAmount) {
    var invoice = new Invoice();
    return Invoice.findById(invoiceId).then(function (result) {
        invoice.id = result.id;
        invoice.number = result.number;
        invoice.clientId = result.clientId;
        invoice.vat = result.vat;
        invoice.amount = parseFloat(result.amount);
        invoice.paidAmount = parseFloat(result.paidAmount);
        
        invoice.paidAmount = invoice.paidAmount + parseFloat(paidAmount);
        return invoice.update().catch(function(error){
            console.log(error);
        });
    });
}

Payment.prototype.save = function () {

    var payment = this;
    var data = {};
    var deferrer = q.defer();

    data = convertToDatabaseColumnNames(payment);

    db.getConnection(function (error, connection) {
        if (error) {
            deferrer.reject(error);
        }

        connection.query("INSERT INTO ?? SET ?", [schema.table, data], function (error, result) {
            connection.release();

            if (error) {
                deferrer.reject(error);
                return;
            }

            updateInvoicePayedAmount(payment.invoiceId, payment.amount).then(function () {
                deferrer.resolve(result);
            }).catch(function (error) {
                deferrer.reject(error);
            });
        });
    });

    return deferrer.promise;
}



Payment.prototype.delete = function () {
    var payment = this;
    var deferrer = q.defer();

    db.getConnection(function (error, connection) {
        if (error) {
            deferrer.reject(error);
        }

        connection.query("DELETE FROM ?? WHERE ??=?", [schema.table, schema.fields.id.name, payment.id], function (error, result) {
            connection.release();

            if (error) {
                deferrer.reject(error);
            }

            deferrer.resolve(result);
        });
    });

    return deferrer.promise;
}

Payment.prototype.update = function () {
    var payment = this;
    var data = {};
    var deferer = q.defer();


    data = convertToDatabaseColumnNames(payment);

    db.getConnection(function (error, connection) {
        connection.release();

        if (error) {
            deferer.reject(error);
        }

        connection.query("UPDATE ?? SET ? WHERE ??=?", [schema.table, data, schema.fields.id.name, payment.id], function (error, result) {
            if (error) {
                deferrer.reject(error);
            }

            updateInvoicePayedAmount(payment.invoiceId, payment.amount).then(function () {
                deferrer.resolve(result);
            }).catch(function (error) {
                deferrer.reject(error);
            });
        });
    });

    return deferer.promise;
}

Payment.findById = function (paymentId) {
    var deferer = q.defer();

    db.getConnection(function (error, connection) {
        if (error) {
            deferer.reject(error);
        }

        connection.query("SELECT * FROM ?? WHERE ??=?", [schema.table, schema.fields.id.name, paymentId], function (error, payment) {
            connection.release();

            if (error) {
                deferer.reject(error);
            }

            deferer.resolve(payment[0]);
        });
    });

    return deferer.promise;
}


Payment.findAll = function () {

    var deferer = q.defer();

    db.getConnection(function (error, connection) {
        if (error) {
            deferer.reject(error);
        }

        connection.query("SELECT * FROM ??", [schema.table], function (error, payments) {
            connection.release();

            if (error) {
                deferer.reject(error);
            }

            deferer.resolve(payments);
        });
    });

    return deferer.promise;
}


Payment.getSchema = function () {
    return schema;
}

Payment.find = function (options) {
    var deferer = q.defer();

    var queryString = "SELECT " + (options.columns.length > 0 ? "??" : "*") + " FROM ?? WHERE ";
    var queryData = [];

    console.log(queryString);

    if (options.columns.length > 0) {
        queryData.push(options.columns);
    }
    queryData.push(schema.table);

    for (condition of options.criteria) {
        if (_.has(condition, "agrCondition")) {
            queryString = queryString + " " + condition.agrCondition;
        } else {
            queryString = queryString + " ? ";
            queryData.push(condition);
        }
    }

    db.getConnection(function (error, connection) {
        if (error) {
            deferer.reject(error);
        }

        connection.query(queryString, queryData, function (error, payments) {
            connection.release();

            if (error) {
                deferer.reject(error);
            }

            deferer.resolve(payments);
        });
    });

    return deferer.promise;
}

