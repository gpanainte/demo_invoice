var route = require('express').Router();
var Payment = require.main.require('./server/models/payment/payment');
var requireLogin = require.main.require('./server/auth/require_login');

module.exports = route;



route.post('/payment',requireLogin, function (request, response) {
    var payment = new Payment();

    payment.invoiceId = request.body.invoiceId;
    payment.clientId = request.body.clientId;
    payment.amount = request.body.amount;

    payment.save().then(function (result) {
        response.send({ status: "OK", data: result });
    }).catch(function (error) {
        response.send({ status: "NOK", data: error });
    });

});

route.put('/payment/:id',requireLogin, function (request, response) {

    var payment = new Payment();

    payment.id = request.params.id;
    payment.invoiceId = request.body.invoiceId;
    payment.clientId = request.body.clientId;
    payment.amount = request.body.amount;

    payment.update().then(function (result) {
        response.send({ status: "OK", data: result });
    }).catch(function (error) {
        response.send({ status: "NOK", data: error });
    });
});

route.get('/payment/:id',requireLogin, function (request, response) {
    Payment.findById(request.params.id).then(function (payment) {
        response.send(payment);
    }).catch(function (error) {
        response.send(error);
    });
});


route.get('/payments',requireLogin, function (request, response) {
    Payment.findAll().then(function (payments) {
        response.send(payments);
    }).catch(function (error) {
        response.send(error);
    });
});
