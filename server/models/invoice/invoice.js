module.exports = Invoice;

var db = require.main.require("./server/providers/db_handler").pool;
var InvoiceLine = require.main.require('./server/models/invoice_line/invoice_line');
var q = require("q");
var _ = require("underscore");

const schema = {
    table: "invoice",
    fields: {
        id: {
            name: "id",
            required: true
        },
        number: {
            name: "number",
            required: true
        },
        clientId: {
            name: "clientId",
            required: true
        },
        vat: {
            name: "vat",
            required: true
        },
        amount: {
            name: "amount",
            required: true
        },
        paidAmount: {
            name: "paid_amount",
            required: false
        }
    }
}

//constructor
function Invoice(number, clientId, vat, amount) {

    var invoice = this;

    invoice.number = number;
    invoice.clientId = clientId;
    invoice.vat = vat;
    invoice.amount = amount;
    invoice.paidAmount = 0;

}

/**
 * Creates an object where property names are same as table column names
 */
var convertToDatabaseColumnNames = function (invoice) {
    var data = {};

    data[schema.fields.number.name] = invoice.number;
    data[schema.fields.clientId.name] = invoice.clientId;
    data[schema.fields.vat.name] = invoice.vat;
    data[schema.fields.amount.name] = invoice.amount;
    data[schema.fields.paidAmount.name] = invoice.paidAmount;

    return data;
}

var convertToSchema = function(rawData){
    var data = {};
    
    _.each(_.keys(schema.fields),function(field){
        data[field] = rawData[schema.fields[field].name];
    });
    
    return data;
}

var saveInvoiceLines = function (invoiceResult, lines) {
    var result = q();
    var allLines = [];

    for (line of lines) {
        var newLine = new InvoiceLine();
        newLine.invoiceId = invoiceResult.insertId;
        newLine.description = line.description;
        newLine.amount = line.amount;
        allLines.push(newLine);
    }

    allLines.forEach(function (line) {
        result = result.then(function () {
            return line.save().catch(function (error) {
                console.log("[ERROR]", error);
            });
        });
    });

    return result;
}

Invoice.prototype.save = function () {

    var invoice = this;
    var data = {};
    var deferrer = q.defer();

    data = convertToDatabaseColumnNames(invoice);

    db.getConnection(function (error, connection) {
        if (error) {
            deferrer.reject(error);
        }

        connection.query("INSERT INTO ?? SET ?", [schema.table, data], function (error, result) {
            connection.release();

            if (error) {
                deferrer.reject(error);
                return;
            }
            deferrer.resolve(result);
        });
    });

    return deferrer.promise;
}

Invoice.prototype.saveWithLines = function (invoceLines) {
    var invoice = this;
    var data = {};
    var deferrer = q.defer();

    data = convertToDatabaseColumnNames(invoice);

    db.getConnection(function (error, connection) {
        if (error) {
            deferrer.reject(error);
        }

        connection.query("INSERT INTO ?? SET ?", [schema.table, data], function (error, result) {
            connection.release();

            if (error) {
                deferrer.reject(error);
                return;
            }

            saveInvoiceLines(result, invoceLines).then(function (result) {
                deferrer.resolve(result);
            }).catch(function (error) {
                deferer.reject(error);
            });

        });
    });

    return deferrer.promise;
}

Invoice.prototype.delete = function () {
    var invoice = this;
    var deferrer = q.defer();

    db.getConnection(function (error, connection) {
        if (error) {
            deferrer.reject(error);
        }

        connection.query("DELETE FROM ?? WHERE ??=?", [schema.table, schema.fields.id.name, invoice.id], function (error, result) {
            connection.release();

            if (error) {
                deferrer.reject(error);
            }

            deferrer.resolve(result);
        });
    });

    return deferrer.promise;
}

Invoice.prototype.update = function () {
    var invoice = this;
    var data = {};
    var deferer = q.defer();

    data = convertToDatabaseColumnNames(invoice);

    db.getConnection(function (error, connection) {
        connection.release();

        if (error) {
            deferer.reject(error);
        }

        connection.query("UPDATE ?? SET ? WHERE ??=?", [schema.table, data, schema.fields.id.name, invoice.id], function (error, result) {
            if (error) {
                deferrer.reject(error);
            }

            deferer.resolve(result);
        });
    });

    return deferer.promise;
}

Invoice.findById = function (invoiceId) {
    var deferer = q.defer();

    db.getConnection(function (error, connection) {
        if (error) {
            deferer.reject(error);
        }

        connection.query("SELECT * FROM ?? WHERE ??=?", [schema.table, schema.fields.id.name, invoiceId], function (error, invoice) {
            connection.release();

            if (error) {
                deferer.reject(error);
            }
            
            invoice = convertToSchema(invoice[0]);
            deferer.resolve(invoice);
        });
    });

    return deferer.promise;
}


Invoice.findAll = function () {

    var deferer = q.defer();
    
    db.getConnection(function (error, connection) {
        if (error) {
            deferer.reject(error);
        }

        connection.query("SELECT * FROM ??", [schema.table], function (error, invoices) {
            connection.release();

            if (error) {
                deferer.reject(error);
            }
            
            for(var i = 0; i<invoices.length; i++){
                invoices[i]=convertToSchema(invoices[i]);
            }

            deferer.resolve(invoices);
        });
    });

    return deferer.promise;
}


Invoice.getSchema = function () {
    return schema;
}

Invoice.find = function (options) {
    var deferer = q.defer();

    var queryString = "SELECT " + (options.columns.length > 0 ? "??" : "*") + " FROM ?? WHERE ";
    var queryData = [];

    if (options.columns.length > 0) {
        queryData.push(options.columns);
    }
    queryData.push(schema.table);

    for (condition of options.criteria) {
        if (_.has(condition, "agrCondition")) {
            queryString = queryString + " " + condition.agrCondition;
        } else {
            queryString = queryString + " ? ";
            queryData.push(condition);
        }
    }

    db.getConnection(function (error, connection) {
        if (error) {
            deferer.reject(error);
        }

        connection.query(queryString, queryData, function (error, invoices) {
            connection.release();

            if (error) {
                deferer.reject(error);
            }
            
            for(var i = 0; i<invoices.length; i++){
                invoices[i]=convertToSchema(invoices[i]);
            }

            deferer.resolve(invoices);
        });
    });

    return deferer.promise;
}

var getInvoice = function(invoiceId){
    return Invoice.findById(invoiceId).then(function (invoice) {       
        return invoice;
    })
}

var getInvoiceLines = function(invoice){
    var data = {invoice: invoice,lines:[]};
    return InvoiceLine.findByInvoiceId(invoice.id).then(function(invoiceLines){
        data.lines = invoiceLines;
        return data;
    });
}

Invoice.getInvoiceWithLines = function(invoiceId){
   return getInvoice(invoiceId).then(getInvoiceLines);
}