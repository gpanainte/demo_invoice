var route = require('express').Router();
var Invoice = require.main.require('./server/models/invoice/invoice');
var InvoiceLine = require.main.require('./server/models/invoice_line/invoice_line');
var requireLogin = require.main.require('./server/auth/require_login');

module.exports = route;



route.post('/invoice',requireLogin, function (request, response) {
    var invoice = new Invoice();

    invoice.number = request.body.number;
    invoice.clientId = request.body.clientId;
    invoice.vat = request.body.vat;
    invoice.amount = request.body.amount;

    invoice.saveWithLines(request.body.lines).then(function (result) {
        response.send({ status: "OK", data: result });
    }).catch(function (error) {
        response.send({ status: "NOK", data: error });
    });

});

route.put('/invoice/:id',requireLogin, function (request, response) {

    var invoice = new Invoice();

    invoice.id = request.params.id;
    invoice.number = request.body.number;
    invoice.clientId = request.body.clientId;
    invoice.vat = request.body.vat;
    invoice.amount = request.body.amount;

    invoice.update().then(function (result) {
        response.send({ status: "OK", data: result });
    }).catch(function (error) {
        response.send({ status: "NOK", data: error });
    });
});

route.get('/invoice/:id',requireLogin, function (request, response) {
    Invoice.findById(request.params.id).then(function (invoice) {
        response.send(invoice);
    }).catch(function (error) {
        response.send(error);
    });
});

route.get('/invoice/:id/lines',requireLogin, function (request, response) {
    
    Invoice.getInvoiceWithLines(request.params.id).then(function(result){
        response.send(result);
    }).catch(function (error) {
        response.send(error);
    });
});

route.get('/invoices',requireLogin, function (request, response) {
    Invoice.findAll().then(function (invoices) {
        response.send(invoices);
    }).catch(function (error) {
        response.send(error);
    });
});
