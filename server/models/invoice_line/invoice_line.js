module.exports = InvoiceLine;

var db = require.main.require("./server/providers/db_handler").pool;
var q = require("q");

const schema = {
    table : "invoice_lines",
    fields : {
        id : {
            name:"id",
            required:true
        },
        invoiceId : {
            name : "invoiceId",
            required: true
        },
        description : {
            name:"description",
            required: false
        },
        amount : {
            name:"amount",
            required: true
        }
    }
}

//constructor
function InvoiceLine(invoiceId,description,amount){
    
    var invoiceLine = this;
    
    invoiceLine.invoiceId = invoiceId;
    invoiceLine.description = description;
    invoiceLine.amount = amount;   
    
}

InvoiceLine.getSchema = function(){
    return schema;
}

/**
 * Creates an object where property names are same as table column names
 */
var convertToDatabaseColumnNames = function(invoiceLine){
    var data = {};
    
    data[schema.fields.invoiceId.name] = invoiceLine.invoiceId;
    data[schema.fields.description.name] = invoiceLine.description; 
    data[schema.fields.amount.name] = invoiceLine.amount;
    
    return data;
}

var convertToSchema = function(rawData){
    var data = {};
    
    _.each(_.keys(schema.fields),function(field){
        data[field] = rawData[schema.fields[field].name];
    });
    
    return data;
}

InvoiceLine.prototype.save = function(){
    
    var invoiceLine = this;
    var data = {};
    var deferrer = q.defer();
    
    data = convertToDatabaseColumnNames(invoiceLine);
    
    db.getConnection(function(error,connection){
        if(error) {
            deferrer.reject(error);
        }
        
        connection.query("INSERT INTO ?? SET ?",[schema.table,data], function(error,result){
            connection.release();
            
            if(error) {
                deferrer.reject(error);
            }
            
            deferrer.resolve(result);
        });
    });

    return deferrer.promise;    
}

InvoiceLine.prototype.delete = function(){
    var invoiceLine = this;
    var deferrer = q.defer();
    
    db.getConnection(function(error,connection){
       if(error){
           deferrer.reject(error);
       } 
       
       connection.query("DELETE FROM ?? WHERE ??=?",[schema.table,schema.fields.id.name,invoiceLine.id],function(error,result){
           connection.release();
           
           if(error){
               deferrer.reject(error);
           }
           
           deferrer.resolve(result);
       });
    });
    
    return deferrer.promise;
}

InvoiceLine.prototype.update = function(){
    var invoiceLine = this;
    var data = {};
    var deferer = q.defer();
    
    
    data = convertToDatabaseColumnNames(invoiceLine);
    
    db.getConnection(function(error,connection){
        connection.release();
        
        if(error){
            deferer.reject(error);
        }
        
        connection.query("UPDATE ?? SET ? WHERE ??=?",[schema.table,data,schema.fields.id.name,invoiceLine.id],function(error, result){
            if(error){
                deferrer.reject(error);
            }
            
            deferer.resolve(result);
        });
    });
    
    return deferer.promise;
}

InvoiceLine.findByInvoiceId = function(invoiceId){
    var deferer = q.defer();
    
    db.getConnection(function(error,connection){
        if(error){
            deferer.reject(error);
        }
        
        connection.query("SELECT * FROM ?? WHERE ??=?",[schema.table,schema.fields.invoiceId.name,invoiceId],function (error,invoiceLines) {
           connection.release();
           
           if(error){
               deferer.reject(error);
           } 
           
           deferer.resolve(invoiceLines);
        });
    });
    
    return deferer.promise;
}



InvoiceLine.findAll = function(){
    
    var deferer = q.defer();
    
    db.getConnection(function (error,connection) {
        if(error){
            deferer.reject(error);
        }
        
        connection.query("SELECT * FROM ??",[schema.table],function (error,invoiceLines) {
            connection.release();
            
            if(error){
                deferer.reject(error);
            }
            
            deferer.resolve(invoiceLines);
        });
    });
   
   return deferer.promise; 
}

InvoiceLine.find = function (options) {
    var deferer = q.defer();

    var queryString = "SELECT " + (options.columns.length > 0 ? "??" : "*") + " FROM ?? WHERE ";
    var queryData = [];

    console.log(queryString);

    if (options.columns.length > 0) {
        queryData.push(options.columns);
    }
    queryData.push(schema.table);

    for (condition of options.criteria) {
        if (_.has(condition, "agrCondition")) {
            queryString = queryString + " " + condition.agrCondition;
        } else {
            queryString = queryString + " ? ";
            queryData.push(condition);
        }
    }

    db.getConnection(function (error, connection) {
        if (error) {
            deferer.reject(error);
        }

        connection.query(queryString, queryData, function (error, invoiceLines) {
            connection.release();

            if (error) {
                deferer.reject(error);
            }

            deferer.resolve(invoiceLines);
        });
    });

    return deferer.promise;
}