var mysql = require("mysql");
var pool = mysql.createPool({
    connectionLimit: 100,
    host: 'localhost',
    user: 'demo_invoice',
    password: 'Demo_invoice123',
    database: 'demo_invoice',
    port: 3306,
    debug: ['ComQueryPacket', 'RowDataPacket']
});

exports.pool = pool;
