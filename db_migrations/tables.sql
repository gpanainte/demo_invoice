CREATE TABLE `demo_invoice`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `role` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `demo_invoice`.`users` (`name`, `password`, `role`) VALUES ('admin', 'test', 'admin');

CREATE TABLE `demo_invoice`.`invoice` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(45) NOT NULL,
  `clientId` INT NOT NULL,
  `vat` INT NOT NULL,
  `amount` DECIMAL(5,2) NOT NULL,
  `paid_amount` DECIMAL(5,2) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `number_UNIQUE` (`number` ASC));


CREATE TABLE `demo_invoice`.`invoice_lines` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `invoiceId` INT NOT NULL,
  `description` VARCHAR(45) NULL,
  `amount` DECIMAL(5,2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`));


CREATE TABLE `demo_invoice`.`payment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `invoiceId` INT NOT NULL,
  `clientId` INT NOT NULL,
  `amount` DECIMAL(5,2) NOT NULL,
  `date` DATETIME NOT NULL,
  PRIMARY KEY (`id`));
